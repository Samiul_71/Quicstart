@extends('layouts.app')

@section('content')

    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Tasks
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                    <th>Task</th>

                    </thead>

                    <!-- Table Body -->
                    <tbody>
                    @foreach ($tasks as $task)
                        <tr>
                            <!-- Task Name -->
                            <td class="table-text" style="border:dashed";>
                                <div>{{ $task->name }}</div>
                            </td>

                            <td>
                                <!-- TODO: Delete Button -->
                                <form action="task/{{ $task->id }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button>Delete Task</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    {{--<a href="add.blade.php">Add</a>--}}
                    </tbody>
                    <a href="{{ URL::to('task/create') }}">
                        Create
                    </a>
                </table>
            </div>
        </div>
    @endif



@endsection